package com.example.milancas.productapi.mappers;


import com.example.milancas.productapi.dto.ProductDto;
import com.example.milancas.productapi.persistance.entity.Product;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductDto productToDto(Product product);

    List<ProductDto> toProductDtos(List<Product> products);

    Product toProduct(ProductDto productDto);

}
