package com.example.milancas.productapi.mappers.patch;

import com.example.milancas.productapi.dto.patch.ProductPatchDto;
import com.example.milancas.productapi.persistance.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProductPatchMapper {

    void updateProductFromPatchDto(ProductPatchDto productPatchDto, @MappingTarget Product entity);

}
