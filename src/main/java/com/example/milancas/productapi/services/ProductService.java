package com.example.milancas.productapi.services;

import com.example.milancas.productapi.dto.ProductDto;
import com.example.milancas.productapi.dto.log.LogStatusDto;
import com.example.milancas.productapi.dto.patch.ProductPatchDto;
import com.example.milancas.productapi.mappers.ProductMapper;
import com.example.milancas.productapi.mappers.patch.ProductPatchMapper;
import com.example.milancas.productapi.message.MessagingConfig;
import com.example.milancas.productapi.persistance.repository.ProductRepository;
import com.example.milancas.productapi.persistance.entity.Product;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductService {

    private ProductRepository productRepository;
    private ProductMapper productMapper;
    private ProductPatchMapper productPatchMapper;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper, ProductPatchMapper productPatchMapper){
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.productPatchMapper = productPatchMapper;
    }

    public ProductDto saveProduct(ProductDto productDto) {
        Product newProduct = productMapper.toProduct(productDto);
        productRepository.save(newProduct);
        return productMapper.productToDto(newProduct);
    }

    public List<ProductDto> getProducts() {
        return productMapper.toProductDtos(productRepository.findAll());
    }

    public ProductDto getProductById(UUID id) {
        return productMapper.productToDto(productRepository.findById(id).orElse(null));
    }

    public ProductDto deleteProduct(UUID id) {
        Product deletedProduct = productRepository.findById(id).orElse(null);
        if (deletedProduct == null) {
            return null;
        }
        else {
            productRepository.deleteById(id);
            return productMapper.productToDto(deletedProduct);
        }
    }

    public ProductDto updateProduct(ProductPatchDto productPatchDto, UUID id) {
        Product existingProduct = productRepository.findById(id).orElse(null);
        if (existingProduct == null) {
            return null;
        }
        else {
            productPatchMapper.updateProductFromPatchDto(productPatchDto, existingProduct);
        }
        return productMapper.productToDto(productRepository.save(existingProduct));
    }

    public LogStatusDto buildLogStatusDto(String level, String message) {
        LogStatusDto logStatusDto = new LogStatusDto();
        logStatusDto.setMessage(message);
        logStatusDto.setLevel(level);
        logStatusDto.setDateAndTime(new Date());
        logStatusDto.setServiceName("Product-API");
        return logStatusDto;
    }
}
