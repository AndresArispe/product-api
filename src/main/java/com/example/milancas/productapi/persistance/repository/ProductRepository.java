package com.example.milancas.productapi.persistance.repository;

import com.example.milancas.productapi.persistance.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository <Product, UUID> {
}
