package com.example.milancas.productapi.persistance.entity;

import com.vladmihalcea.hibernate.type.array.StringArrayType;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.util.UUID;

@Data
@TypeDef(
        name = "string-array",
        typeClass = StringArrayType.class
)

@Entity
@Table(name = "products") //minusculas y guion bajo

public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(
            name = "name"
    )
    private String name;

    @Type(type = "string-array")
    @Column(
            name = "description",
            columnDefinition = "text[]"
    )
    private String[] description;

    @Column(
            name = "company_id"
    )
    private UUID companyId;

    @Column(
            name = "blocked"
    )
    private Boolean blocked;

    @Type(type = "string-array")
    @Column(
            name = "categories",
            columnDefinition = "text[]"
    )
    private String[] categories;


}
