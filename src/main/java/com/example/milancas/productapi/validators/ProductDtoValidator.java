package com.example.milancas.productapi.validators;

import com.example.milancas.productapi.dto.ProductDto;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ProductDtoValidator implements Validator {

    @Override
    public boolean supports(Class clazz) {
        return ProductDto.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        ProductDto productDto = (ProductDto) obj;
    }
}
