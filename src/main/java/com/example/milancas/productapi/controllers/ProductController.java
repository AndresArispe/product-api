package com.example.milancas.productapi.controllers;

import com.example.milancas.productapi.dto.log.LogStatusDto;
import com.example.milancas.productapi.dto.patch.ProductPatchDto;
import com.example.milancas.productapi.errors.ErrorResponse;
import com.example.milancas.productapi.dto.ProductDto;
import com.example.milancas.productapi.message.MessagingConfig;
import com.example.milancas.productapi.services.ProductService;
import com.example.milancas.productapi.validators.ProductDtoValidator;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class ProductController {

    private LogStatusDto logStatusDto = new LogStatusDto();

    private ProductService productService;
    private RabbitTemplate template;

    public ProductController(ProductService productService, RabbitTemplate template) {
        this.productService = productService;
        this.template = template;
    }

    @GetMapping("/v1/products")
    public List<ProductDto> getAllProducts() {
        logStatusDto = productService.buildLogStatusDto("info", "get products list");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return productService.getProducts();
    }

    @GetMapping("/v1/products/{id}")
    public ProductDto getProduct(@PathVariable UUID id) {
        if (productService.getProductById(id) == null) {
            logStatusDto = productService.buildLogStatusDto("error", "get product by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "product not found"
            );
        }
        else {
            logStatusDto = productService.buildLogStatusDto("info", "get product by id");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return productService.getProductById(id);
        }
    }

    @GetMapping("/v1/products/healthcheck")
    public String healthCheck() {
        logStatusDto = productService.buildLogStatusDto("info", "get healthcheck");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        return "I'm Alive";
    }

    @PostMapping(value = "/v1/products")
    public ResponseEntity<Object> addProduct(@Valid @RequestBody ProductDto productDto, BindingResult result) {
        new ProductDtoValidator().validate(productDto, result);
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = productService.buildLogStatusDto("error", "adding product");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            // Here you can change ok to badRequest depending on your use case.
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation failure", errors));
            // In case if you want to fail the request, you need to use the below:
            // return ResponseEntity.badRequest().body(new ErrorResponse("404", "Validation failure", errors));
        }
        logStatusDto = productService.buildLogStatusDto("info", "adding product");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        ProductDto created = productService.saveProduct(productDto);
        return ResponseEntity.ok(created);
    }

    @PatchMapping(value = "/v1/products/{productId}")
    public ResponseEntity<Object> updateProduct(@Valid @RequestBody ProductPatchDto productPatchDto, @PathVariable UUID productId, BindingResult result) {
        if (productService.getProductById(productId) == null) {
            logStatusDto = productService.buildLogStatusDto("error", "updating product");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "product not found"
            );
        }
        if (result.hasErrors()) {
            List<String> errors = result.getAllErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
            logStatusDto = productService.buildLogStatusDto("error", "updating product");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.badRequest().body(new ErrorResponse("400", "Validation failure", errors));
        }
        logStatusDto = productService.buildLogStatusDto("info", "updating product");
        template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
        ProductDto patchProductDto = productService.updateProduct(productPatchDto, productId);
        return ResponseEntity.ok(patchProductDto);
    }

    @DeleteMapping(value = "/v1/products/{productId}")
    public ResponseEntity deleteProduct(@PathVariable UUID productId) {
        if (productService.deleteProduct(productId) == null) {
            logStatusDto = productService.buildLogStatusDto("error", "deleting product");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "product not found"
            );
        }
        else {
            logStatusDto = productService.buildLogStatusDto("info", "deleting product");
            template.convertAndSend(MessagingConfig.EXCHANGE, MessagingConfig.ROUTING_KEY, logStatusDto);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getFieldErrors().forEach(error ->
                errors.put(error.getField(), error.getDefaultMessage()));
        return errors;
    }
}
