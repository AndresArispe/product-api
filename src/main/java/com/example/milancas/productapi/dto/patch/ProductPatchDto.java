package com.example.milancas.productapi.dto.patch;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class ProductPatchDto {

    private UUID id;

    @Size(min = 5, max = 10, message = "Type String max 15 characters and min 5 characters")
    private String name;

    @Size(min = 1, max = 10, message = "You must send at least one description and max ten descriptions")
    private String [] description;

    private UUID companyId;

    private transient Boolean blocked;

    @Size(min = 1, max = 10, message = "You must send at least one category and max ten categories")
    private String [] categories;

}
