package com.example.milancas.productapi.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
public class ProductDto {

    private UUID id;

    @NotNull(message = "Name it's required")
    @Size(min = 5, max = 10, message = "Type String max 15 characters and min 5 characters")
    private String name;

    @NotNull(message = "Description it's required")
    @Size(min = 1, max = 10, message = "You must send at least one description and max ten descriptions")
    private String [] description;

    @NotNull(message = "Company Id it's required")
    private UUID companyId;

    @NotNull(message = "Blocked must be type Boolean")
    private transient Boolean blocked;

    @NotNull(message = "Categories it's required")
    @Size(min = 1, max = 10, message = "You must send at least one category and max ten categories")
    private String [] categories;

}
