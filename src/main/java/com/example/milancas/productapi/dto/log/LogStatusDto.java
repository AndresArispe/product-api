package com.example.milancas.productapi.dto.log;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class LogStatusDto {

    private String level;
    private Date dateAndTime;
    private String serviceName;
    private String message;

}
